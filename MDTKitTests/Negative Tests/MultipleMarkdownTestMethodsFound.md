MultipleMarkdownTestMethodsFoundTest
------------------------------------

This test tests the behavior if multiple similar named test methods were defined in a markdown test file. It expects a `MDParserException("multiple tests matching 'MultipleMarkdownTestMethodsFoundTest' found")` Exception.

| var:number |
|:----------:|
|     0      |

MultipleMarkdownTestMethodsFoundTest
------------------------------------

See above.

| var:number |
|:----------:|
|     0      |
