DoublePreConstantAssignmentsTest
--------------------------------

This test tests the behavior of double assignments to pre constants:

It expects a `MDParserException("variable 'double_assigned_var' previously declared")` Exception.

```
pre double_assigned_var:number = 0;
pre double_assigned_var:number = 3;
```

| var:number |
|:----------:|
|     0      |
