$IMPORT_ONLY$

This file is basically used for testing the linkage capability of MDTKit

```
const LINKED_NUMBER = -4;
const LINKED_DOUBLE_NUMBER = 4.3;
const LINKED_BOOLEAN = true;
const LINKED_STRING = "what a string again!";
const LINKED_STRING_ML =

"awesome how
this string
spreads across
several lines!";

const LINKED_DATE = 2015-04-27T21:43:53Z;
const LINKED_NULL = [null];

```
